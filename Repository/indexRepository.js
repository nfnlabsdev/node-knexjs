module.exports = function () {
    require('dotenv').config()
    const config = {
      client: 'mysql2',
      connection: {
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        password: process.env.DB_PASS,
        database: process.env.DB_NAME
      },
      pool: {
        min: Number(process.env.DB_POOL_MIN),
        max: Number(process.env.DB_POOL_MAX)
      },
      acquireConnectionTimeout: Number(process.env.DB_TIMEOUT)
    }
    const Knex = require('knex')

    const student = "students"
    const staff = "staff"
    const studentMarks = "students_marks"


    // Insert
    this.addStudent = (data) => {
        var response = {}
        return new Promise(function (resolve) {
          var knex = new Knex(config)
          knex(student)
            .insert(data)
            .then((result) => {
              if (result[0] > 0) {
                response.error = false
                response.result = result[0]
              } else {
                response.error = true
                response.result = null
              }
              resolve(response)
            })
            .catch((err) => {
              err.error = true
              resolve(err)
            })
            .finally(() => {
              knex.destroy()
            })
        })
      }

      this.addStudentMarks = (data) => {
        var response = {}
        return new Promise(function (resolve) {
          var knex = new Knex(config)
          knex(studentMarks)
            .insert(data)
            .then((result) => {
              if (result[0] > 0) {
                response.error = false
                response.result = result[0]
              } else {
                response.error = true
                response.result = null
              }
              resolve(response)
            })
            .catch((err) => {
              err.error = true
              resolve(err)
            })
            .finally(() => {
              knex.destroy()
            })
        })
      }
      this.addStaff = (data) => {
        var response = {}
        return new Promise(function (resolve) {
          var knex = new Knex(config)
          knex(staff)
            .insert(data)
            .then((result) => {
              if (result[0] > 0) {
                response.error = false
                response.result = result[0]
              } else {
                response.error = true
                response.result = null
              }
              resolve(response)
            })
            .catch((err) => {
              err.error = true
              resolve(err)
            })
            .finally(() => {
              knex.destroy()
            })
        })
      }



      // Select
      this.getStudent = (data) => {
        var response = {}
        return new Promise(function (resolve) {
          var knex = new Knex(config)
          knex(student)
            .select('*')
            .where(data)
            .then((result) => {
              if (result.length > 0) {
                response.error = false
                response.result = result
              } else {
                response.error = true
              }
              resolve(response)
            })
            .catch((err) => {
              err.error = true
              resolve(err)
            })
            .finally(() => {
              knex.destroy()
            })
        })
      }


      this.search = (name) => {
        var response = {}
        return new Promise(function (resolve) {
          var knex = new Knex(config)
          knex(student)
            .select('*')
            .where('name', 'like', `%${name}%`)
            .then((result) => {
              if (result.length > 0) {
                response.error = false
                response.result = result
              } else {
                response.error = true
              }
              resolve(response)
            })
            .catch((err) => {
              err.error = true
              resolve(err)
            })
            .finally(() => {
              knex.destroy()
            })
        })
      }
      this.getStaff = (data) => {
        var response = {}
        return new Promise(function (resolve) {
          var knex = new Knex(config)
          knex(staff)
            .select('*')
            .where(data)
            .then((result) => {
              if (result.length > 0) {
                response.error = false
                response.result = result
              } else {
                response.error = true
              }
              resolve(response)
            })
            .catch((err) => {
              err.error = true
              resolve(err)
            })
            .finally(() => {
              knex.destroy()
            })
        })
      }
// join
      this.getJoin = (data) => {
        var response = {}
        return new Promise(function (resolve) {
          var knex = new Knex(config)
          knex(student)
            .join(studentMarks, `${studentMarks}.student_id`, `${student}.id`)
            .where(data)
            .then((result) => {
              if (result.length > 0) {
                response.error = false
                response.result = result
              } else {
                response.error = true
              }
              resolve(response)
            })
            .catch((err) => {
              err.error = true
              resolve(err)
            })
            .finally(() => {
              knex.destroy()
            })
        })
      }
// inner join
      this.getInnerJoin = (data) => {
        var response = {}
        return new Promise(function (resolve) {
          var knex = new Knex(config)
          knex(student)
          .innerJoin(studentMarks, `${studentMarks}.student_id`, `${student}.id`)
            .where(data)
            .then((result) => {
              if (result.length > 0) {
                response.error = false
                response.result = result
              } else {
                response.error = true
              }
              resolve(response)
            })
            .catch((err) => {
              err.error = true
              resolve(err)
            })
            .finally(() => {
              knex.destroy()
            })
        })
      }
// left join
      this.getLeftJoin = (data) => {
        var response = {}
        return new Promise(function (resolve) {
          var knex = new Knex(config)
          knex(student)
            .leftJoin(studentMarks, `${studentMarks}.student_id`, `${student}.id`)
            .where(data)
            .then((result) => {
              if (result.length > 0) {
                response.error = false
                response.result = result
              } else {
                response.error = true
              }
              resolve(response)
            })
            .catch((err) => {
              err.error = true
              resolve(err)
            })
            .finally(() => {
              knex.destroy()
            })
        })
      }
// right join
      this.getRightJoin = (data) => {
        var response = {}
        return new Promise(function (resolve) {
          var knex = new Knex(config)
          knex(student)
            .rightJoin(studentMarks, `${studentMarks}.student_id`, `${student}.id`)
            .where(data)
            .then((result) => {
              if (result.length > 0) {
                response.error = false
                response.result = result
              } else {
                response.error = true
              }
              resolve(response)
            })
            .catch((err) => {
              err.error = true
              resolve(err)
            })
            .finally(() => {
              knex.destroy()
            })
        })
      }

      // cross join
      this.getCrossJoin = (data) => {
        var response = {}
        return new Promise(function (resolve) {
          var knex = new Knex(config)
          knex(student)
            .crossJoin(studentMarks, `${studentMarks}.student_id`, `${student}.id`)
            .where(data)
            .then((result) => {
              if (result.length > 0) {
                response.error = false
                response.result = result
              } else {
                response.error = true
              }
              resolve(response)
            })
            .catch((err) => {
              err.error = true
              resolve(err)
            })
            .finally(() => {
              knex.destroy()
            })
        })
      }
// distinct column
      this.getDistinctStaffName = () => {
        var response = {}
        return new Promise(function (resolve) {
          var knex = new Knex(config)
          knex(staff)
            .select('*')
            .distinct('name')
            .then((result) => {
              if (result.length > 0) {
                response.error = false
                response.result = result
              } else {
                response.error = true
              }
              resolve(response)
            })
            .catch((err) => {
              err.error = true
              resolve(err)
            })
            .finally(() => {
              knex.destroy()
            })
        })
      }
// group by column
this.getGroupByStudentName = () => {
  var response = {}
  return new Promise(function (resolve) {
    var knex = new Knex(config)
    knex(student)
      .select('*')
      .groupBy('name')
      .then((result) => {
        if (result.length > 0) {
          response.error = false
          response.result = result
        } else {
          response.error = true
        }
        resolve(response)
      })
      .catch((err) => {
        err.error = true
        resolve(err)
      })
      .finally(() => {
        knex.destroy()
      })
  })
}
// order by column
this.getOrderByStudentName = () => {
  var response = {}
  return new Promise(function (resolve) {
    var knex = new Knex(config)
    knex(student)
      .select('*')
      .orderBy('userName')
      .then((result) => {
        if (result.length > 0) {
          response.error = false
          response.result = result
        } else {
          response.error = true
        }
        resolve(response)
      })
      .catch((err) => {
        err.error = true
        resolve(err)
      })
      .finally(() => {
        knex.destroy()
      })
  })
}

// limit and offset by column
this.getStudentsByLimit = (page) => {
  var response = {}
  let offset = ( page - 1 ) * 10
  return new Promise(function (resolve) {
    var knex = new Knex(config)
    knex(student)
      .select('*')
      .limit(10)
      .offset(offset)
      .then((result) => {
        if (result.length > 0) {
          response.error = false
          response.result = result
        } else {
          response.error = true
        }
        resolve(response)
      })
      .catch((err) => {
        err.error = true
        resolve(err)
      })
      .finally(() => {
        knex.destroy()
      })
  })
}
// student count
this.getStudentsCount = () => {
  var response = {}
  return new Promise(function (resolve) {
    var knex = new Knex(config)
    knex(student)
      .select('*')
      .count('* as count')
      // .count('id', {as: 'count'})
      // .count({ count: 'id' })
      .then((result) => {
        if (result.length > 0) {
          response.error = false
          response.result = result
        } else {
          response.error = true
        }
        resolve(response)
      })
      .catch((err) => {
        err.error = true
        resolve(err)
      })
      .finally(() => {
        knex.destroy()
      })
  })
}
// staff count
this.getStaffCount = () => {
  var response = {}
  return new Promise(function (resolve) {
    var knex = new Knex(config)
    knex(student)
      .select('*')
      .count('* as count')
      // eg .count({ a: 'id', v: 'name' })
      // .count({ count: ['id', 'name'] })
      .then((result) => {
        if (result.length > 0) {
          response.error = false
          response.result = result
        } else {
          response.error = true
        }
        resolve(response)
      })
      .catch((err) => {
        err.error = true
        resolve(err)
      })
      .finally(() => {
        knex.destroy()
      })
  })
}
// student min
this.getStudentsMinCount = () => {
  var response = {}
  return new Promise(function (resolve) {
    var knex = new Knex(config)
    knex(student)
      .select('*',knex.raw(`year(NOW()) - year(dob) as age`))
      .havingRaw('min(age)')
      // .having('age', 'min'knex.min({ stu_age: 'age'}))
      .then((result) => {
        console.log(result)
        if (result.length > 0) {
          response.error = false
          response.result = result
        } else {
          response.error = true
        }
        resolve(response)
      })
      .catch((err) => {
        console.log(err)
        err.error = true
        resolve(err)
      })
      .finally(() => {
        knex.destroy()
      })
  })
}

    //   this.addOtp = (data) => {
    //     return new Promise(function (resolve) {
    //       var knex = new Knex(config)
    //       knex(userOtp)
    //         .insert(data)
    //         .then((result) => {
    //           if (result[0] > 0) {
    //             resolve(false)
    //           } else {
    //             resolve(true)
    //           }
    //         })
    //         .catch((err) => {
    //           err = true
    //           resolve(err)
    //         })
    //         .finally(() => {
    //           knex.destroy()
    //         })
    //     })
    //   }

    // Update
    this.updateStudent = (data, condition) => {
        var response = {}
        return new Promise(function (resolve) {
          var knex = new Knex(config)
          knex(student)
            .update(data)
            .where(condition)
            .then((result) => {
              if (result) {
                response.error = false
                resolve(response)
              } else {
                response.error = true
                resolve(response)
              }
            })
            .catch((err) => {
              err.error = true
              resolve(err)
            })
            .finally(() => {
              knex.destroy()
            })
        })
      }
    
    // Delete
      this.removeStudent = (data) => {
        var response = {}
        return new Promise(function (resolve) {
          var knex = new Knex(config)
          knex(student)
            .where(data)
            .delete()
            .then((result) => {
                if (result) {
                    response.error = false
                } else {
                    response.error = true
                }
              resolve(response)
            })
            .catch((err) => {
              err.error = true
              resolve(err)
            })
            .finally(() => {
              knex.destroy()
            })
        })
      }

}