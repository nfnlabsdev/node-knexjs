const express = require('express')
const app = express()
const cors = require('cors')
app.use(cors())

// const path = require('path')

// app.use('/', express.static(path.join(__dirname, '/public')))

const bodyParser = require('body-parser')

const { check, validationResult } = require('express-validator')
const http = require('http').Server(app)

require('dotenv').config()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use('/', function (request, response, next) {
  request.headers.lang = request.headers.lang || 'default'
  console.log(`IP: ${request.connection.remoteAddress} Method: ${request.method} Route: ${request.originalUrl} Body: ` + JSON.stringify(request.body))
  next()
})

const validator = {}
validator.check = check
validator.validation = validationResult

require('./Routes/knexRoutes')(app, validator)

http.listen(process.env.PORT, function () {
  console.log('Server is running on ' + process.env.HOST + ':' + process.env.PORT)
})
