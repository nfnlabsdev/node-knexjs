module.exports = function (app, validator) {

    require('../Repository/indexRepository')()
    const knexApi = "/api/"


    app.post(knexApi + "addStudent", [
        validator.check('name').isLength({min: 1}).withMessage('Missing Name'),
        validator.check('userName').isLength({min: 1}).withMessage('Missing User Name'),
        validator.check('dob').isLength({min: 6}).withMessage('Missing DOB'),
        validator.check('mobile').not().isEmpty().withMessage('Missing Mobile').isLength({min: 8}).withMessage('Mobile Number length between 8 to 15'),
        validator.check('password').not().isEmpty().withMessage('Missing Password').isLength({min: 6, max: 8}).withMessage('Password length should be 6 to 8')

    ], async (request, response) => {
        const error = validator.validation(request)

        if (error.array().length) {
            let msg = error.array()[0].msg
            return response.send({Error: true, Message: msg})
        } else {
            let studentData = request.body
            let studentResult = await this.addStudent(studentData)

            if (studentResult.error) {
                return response.send({Error: studentResult.error, msg: "Insertion Failure"})
            } else {
                return response.send({Error: studentResult.error, msg: "Insertion success", studentID:studentResult.result})
            }
        }
    })

    app.get(knexApi + "getStudent", [
        validator.check('userName').optional(),
    ], async (request, response) => {
        const error = validator.validation(request)

        if (error.array().length) {
            let msg = error.array()[0].msg
            return response.send({Error: true, Message: msg})
        } else {
            var getStudentResult
            let studentData = request.body
            if (studentData.userName) {
                 getStudentResult = await this.getStudent(studentData)
            } else {
                 getStudentResult = await this.getStudent({})
            }

            if (getStudentResult.error) {
                return response.send({Error: getStudentResult.error, msg: "No Students Found"})
            } else {
                return response.send({Error: getStudentResult.error, msg: "success", data: getStudentResult.result})
            }
        }
    })

    app.post(knexApi + "updateStudent", [
        validator.check('id').isLength({min: 1}).withMessage('Missing ID'),
        validator.check('name').optional(),
        validator.check('userName').optional(),
        validator.check('dob').optional(),
        validator.check('mobile').optional(),
        validator.check('password').optional()

    ], async (request, response) => {
        const error = validator.validation(request)

        if (error.array().length) {
            let msg = error.array()[0].msg
            return response.send({Error: true, Message: msg})
        } else {
            let studentData = request.body
            let condition = {id: studentData.id}
            let studentResult = await this.updateStudent(studentData, condition)

            if (studentResult.error) {
                return response.send({Error: studentResult.error, msg: "Updation Failure"})
            } else {
                let getStudentResult = await this.getStudent(condition)
                return response.send({Error: studentResult.error, msg: "Updation success", studentData: getStudentResult.result})
            }
        }
    })

    app.post(knexApi + "removeStudent", [
        validator.check('id').isLength({min: 1}).withMessage('Missing ID')
    ], async (request, response) => {
        const error = validator.validation(request)

        if (error.array().length) {
            let msg = error.array()[0].msg
            return response.send({Error: true, Message: msg})
        } else {
            let studentData = request.body
            let studentResult = await this.removeStudent(studentData)

            if (studentResult.error) {
                return response.send({Error: studentResult.error, msg: "Deletion Failure"})
            } else {
                return response.send({Error: studentResult.error, msg: "Deletion success", studentID: studentData.id})
            }
        }
    })
    app.post(knexApi + "addStudentMarks", [
        validator.check('student_id').isLength({min: 1}).withMessage('Missing Student ID'),
        validator.check('tamil').optional(),
        validator.check('english').optional(),
        validator.check('maths').optional(),
        validator.check('science').optional(),
        validator.check('social').optional()
    ], async (request, response) => {
        const error = validator.validation(request)

        if (error.array().length) {
            let msg = error.array()[0].msg
            return response.send({Error: true, Message: msg})
        } else {
            let studentMarkData = request.body
            let fnl_data = {}
            Object.keys(studentMarkData).map((x, i) => {
                if(studentMarkData[x].length !== 0) {
                    fnl_data[x] = studentMarkData[x]
                }
            })
            let studentMarkResult = await this.addStudentMarks(fnl_data)
            if (studentMarkResult.error) {
                return response.send({Error: studentMarkResult.error, msg: "Creation Failure"})
            } else {
                return response.send({Error: studentMarkResult.error, msg: "Creation success"})
            }
        }
    })

    app.post(knexApi + "studentSearch", [
        validator.check('name', 'Name is Required').not().isEmpty()
    ], async (request, response) => {
        const error = validator.validation(request)

        if (error.array().length) {
            let msg = error.array()[0].msg
            return response.send({Error: true, Message: msg})
        } else {

            let studentData = request.body
            
            let getStudentResult = await this.search(studentData.name)

            if (getStudentResult.error) {
                return response.send({Error: getStudentResult.error, msg: "No Students Found"})
            } else {
                return response.send({Error: getStudentResult.error, msg: "success", data: getStudentResult.result})
            }
        }
    })
    app.post(knexApi + "addStaff", [
        validator.check('name').isLength({min: 1}).withMessage('Missing Name'),
        validator.check('userName').isLength({min: 1}).withMessage('Missing User Name'),
        validator.check('dob').isLength({min: 6}).withMessage('Missing DOB'),
        validator.check('mobile').not().isEmpty().withMessage('Missing Mobile').isLength({min: 8}).withMessage('Mobile Number length between 8 to 15'),
        validator.check('password').not().isEmpty().withMessage('Missing Password').isLength({min: 6, max: 8}).withMessage('Password length should be 6 to 8')

    ], async (request, response) => {
        const error = validator.validation(request)

        if (error.array().length) {
            let msg = error.array()[0].msg
            return response.send({Error: true, Message: msg})
        } else {
            let staffData = request.body
            let staffResult = await this.addStaff(staffData)

            if (staffResult.error) {
                return response.send({Error: staffResult.error, msg: "Insertion Failure"})
            } else {
                return response.send({Error: staffResult.error, msg: "Insertion success", staffID:staffResult.result})
            }
        }
    })
    app.get(knexApi + "getStaff", [
        validator.check('userName').optional(),
    ], async (request, response) => {
        const error = validator.validation(request)

        if (error.array().length) {
            let msg = error.array()[0].msg
            return response.send({Error: true, Message: msg})
        } else {
            var getStaffResult
            let staffData = request.body
            if (staffData.userName) {
                 getStaffResult = await this.getStaff(staffData)
            } else {
                 getStaffResult = await this.getStaff({})
            }

            if (getStaffResult.error) {
                return response.send({Error: getStaffResult.error, msg: "No Staff Found"})
            } else {
                return response.send({Error: getStaffResult.error, msg: "success", data: getStaffResult.result})
            }
        }
    })
    app.get(knexApi + "getJoinStudentMarks", [
        validator.check('userName').optional(),
    ], async (request, response) => {
        const error = validator.validation(request)

        if (error.array().length) {
            let msg = error.array()[0].msg
            return response.send({Error: true, Message: msg})
        } else {
            var getStudentResult
            let studentData = request.body
            getStudentResult = await this.getJoin(studentData)

            if (getStudentResult.error) {
                return response.send({Error: getStudentResult.error, msg: "No Staff Found"})
            } else {
                return response.send({Error: getStudentResult.error, msg: "success", data: getStudentResult.result})
            }
        }
    })
    app.get(knexApi + "getInnerJoinStudentMarks", [
        validator.check('userName').optional(),
    ], async (request, response) => {
        const error = validator.validation(request)

        if (error.array().length) {
            let msg = error.array()[0].msg
            return response.send({Error: true, Message: msg})
        } else {
            var getStudentResult
            let studentData = request.body
            getStudentResult = await this.getInnerJoin(studentData)

            if (getStudentResult.error) {
                return response.send({Error: getStudentResult.error, msg: "No Staff Found"})
            } else {
                return response.send({Error: getStudentResult.error, msg: "success", data: getStudentResult.result})
            }
        }
    })
    app.get(knexApi + "getLeftJoinStudentMarks", [
        validator.check('userName').optional(),
    ], async (request, response) => {
        const error = validator.validation(request)

        if (error.array().length) {
            let msg = error.array()[0].msg
            return response.send({Error: true, Message: msg})
        } else {
            var getStudentResult
            let studentData = request.body
            getStudentResult = await this.getLeftJoin(studentData)

            if (getStudentResult.error) {
                return response.send({Error: getStudentResult.error, msg: "No Staff Found"})
            } else {
                return response.send({Error: getStudentResult.error, msg: "success", data: getStudentResult.result})
            }
        }
    })
    app.get(knexApi + "getRightJoinStudentMarks", [
        validator.check('userName').optional(),
    ], async (request, response) => {
        const error = validator.validation(request)

        if (error.array().length) {
            let msg = error.array()[0].msg
            return response.send({Error: true, Message: msg})
        } else {
            var getStudentResult
            let studentData = request.body
            getStudentResult = await this.getRightJoin(studentData)

            if (getStudentResult.error) {
                return response.send({Error: getStudentResult.error, msg: "No Staff Found"})
            } else {
                return response.send({Error: getStudentResult.error, msg: "success", data: getStudentResult.result})
            }
        }
    })
    app.get(knexApi + "getCrossJoinStudentMarks", [
        validator.check('userName').optional(),
    ], async (request, response) => {
        const error = validator.validation(request)

        if (error.array().length) {
            let msg = error.array()[0].msg
            return response.send({Error: true, Message: msg})
        } else {
            var getStudentResult
            let studentData = request.body
            getStudentResult = await this.getCrossJoin(studentData)
            if (getStudentResult.error) {
                return response.send({Error: getStudentResult.error, msg: "No Staff Found"})
            } else {
                return response.send({Error: getStudentResult.error, msg: "success", data: getStudentResult.result})
            }
        }
    })
    app.get(knexApi + "getStaffDistinctName", 
    // [validator.check('userName').optional(),], 
        async (request, response) => {
        const error = validator.validation(request)

        if (error.array().length) {
            let msg = error.array()[0].msg
            return response.send({Error: true, Message: msg})
        } else {
            var getStaffResult
            // let staffData = request.body
            getStaffResult = await this.getDistinctStaffName()

            if (getStaffResult.error) {
                return response.send({Error: getStaffResult.error, msg: "No Staff Found"})
            } else {
                return response.send({Error: getStaffResult.error, msg: "success", data: getStaffResult.result})
            }
        }
    })
    app.get(knexApi + "getStaffGroupName", 
    // [validator.check('userName').optional(),], 
        async (request, response) => {
        const error = validator.validation(request)

        if (error.array().length) {
            let msg = error.array()[0].msg
            return response.send({Error: true, Message: msg})
        } else {
            var getStaffResult
            // let staffData = request.body
            getStaffResult = await this.getGroupByStudentName()

            if (getStaffResult.error) {
                return response.send({Error: getStaffResult.error, msg: "No Staff Found"})
            } else {
                return response.send({Error: getStaffResult.error, msg: "success", data: getStaffResult.result})
            }
        }
    })
    app.get(knexApi + "getStudentOrderName", 
    // [validator.check('userName').optional(),], 
    async (request, response) => {
        const error = validator.validation(request)

        if (error.array().length) {
            let msg = error.array()[0].msg
            return response.send({Error: true, Message: msg})
        } else {
            // let studentData = request.body
            var getStudentResult = await this.getOrderByStudentName()
            if (getStudentResult.error) {
                return response.send({Error: getStudentResult.error, msg: "No Staff Found"})
            } else {
                return response.send({Error: getStudentResult.error, msg: "success", data: getStudentResult.result})
            }
        }
    })
    app.get(knexApi + "getStudentByPage", 
    [validator.check('page').optional(),], 
    async (request, response) => {
        const error = validator.validation(request)
        if (error.array().length) {
            let msg = error.array()[0].msg
            return response.send({Error: true, Message: msg})
        } else {
            let studentData = request.body
            var getStudentResult = await this.getStudentsByLimit(studentData.page)
            if (getStudentResult.error) {
                return response.send({Error: getStudentResult.error, msg: "No Staff Found"})
            } else {
                return response.send({Error: getStudentResult.error, msg: "success", data: getStudentResult.result})
            }
        }
    })
    app.get(knexApi + "getStudentMinAge", 
    // [validator.check('page').optional(),], 
    async (request, response) => {
        const error = validator.validation(request)
        if (error.array().length) {
            let msg = error.array()[0].msg
            return response.send({Error: true, Message: msg})
        } else {
            // let studentData = request.body
            var getStudentResult = await this.getStudentsMinCount()
            if (getStudentResult.error) {
                return response.send({Error: getStudentResult.error, msg: "No Students Found"})
            } else {
                return response.send({Error: getStudentResult.error, msg: "success", data: getStudentResult.result})
            }
        }
    })



}