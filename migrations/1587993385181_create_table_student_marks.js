module.exports = {
    "up": `CREATE TABLE students_marks (
        id bigint(20) NOT NULL AUTO_INCREMENT,
        student_id bigint(20) DEFAULT NULL,
        english decimal(10,2) DEFAULT 0,
        tamil decimal(10,2) DEFAULT 0,
        maths decimal(10,2) DEFAULT 0,
        science decimal(10,2) DEFAULT 0,
        social decimal(10,2) DEFAULT 0,
        CreateAt timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        UpdateAt timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
        FOREIGN KEY (student_id) REFERENCES students(id),
        PRIMARY KEY (id)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;`,
    "down": `DROP TABLE students_marks`
}