module.exports = {
    "up": `CREATE TABLE staff (
        id bigint(20) NOT NULL AUTO_INCREMENT,
        name varchar(50) DEFAULT NULL,
        userName varchar(50) DEFAULT NULL,
        dob varchar(15) DEFAULT NULL,
        mobile varchar(50) DEFAULT NULL,
        password varchar(50) DEFAULT NULL,
        CreateAt timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        UpdateAt timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (id),
        UNIQUE KEY userName (userName)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;`,
    "down": `DROP TABLE staff`
}