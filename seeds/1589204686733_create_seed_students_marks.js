module.exports = {
    "up": `INSERT INTO students_marks (id, student_id, english, tamil, maths, science, social, CreateAt, UpdateAt)
    VALUES
        (1, 1, 20.00, 10.00, 30.00, 40.00, 40.00, '2020-04-27 19:19:35', NULL),
        (2, 1, 20.00, 10.00, 30.00, 40.00, 0.00, '2020-04-27 19:45:10', NULL),
        (3, 1, 20.00, 10.00, 30.00, 0.00, 0.00, '2020-04-27 19:45:17', NULL);
    `,
    "down": `DELETE FROM students_marks`
}